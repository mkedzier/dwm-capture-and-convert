#pragma once
#include "pch.h"

#include <vector>
#include <string>
#include <chrono> 
#include <iostream>

#include <d3d11.h>
#include <dxgi1_2.h>

#include <iostream>
#include <iomanip>


// Simple class for profiling GPU code (DX11)
class gpu_profiler
{

public:
	gpu_profiler(std::string ProfilerName, ID3D11Device *Device, ID3D11DeviceContext *Context);

	void start();

	void end();

	// dump data in milliseconds (floating point)
	void dump_ms();

	void dump();

	void dump_results_to_file();

	~gpu_profiler();

private:

	ID3D11Query *freqQuery;
	ID3D11Query *startQuery;
	ID3D11Query *endQuery;
	ID3D11Device *device;
	ID3D11DeviceContext *context;
	D3D11_QUERY_DATA_TIMESTAMP_DISJOINT timeStamp;
	INT64 startTime;
	INT64 endTime;
	INT64 minTime;
	INT64 maxTime;
	UINT64 totalTime;
	std::string name;
	INT64 callsCount;
	std::vector<INT64> samples;
};

