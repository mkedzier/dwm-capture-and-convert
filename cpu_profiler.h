#pragma once
#include <vector>
#include <string>
#include <chrono> 
#include <iostream>

// Simple class for profiling CPU code
class cpu_profiler
{
public:
	cpu_profiler(std::string Name);	
	
	void start();
	
	void end();
	long long get_total_time() const;

	void reset();

	void dump();

	void dump_results_to_file();

private:

	std::string name;
	std::chrono::steady_clock::time_point begin;
	std::chrono::steady_clock::time_point endTime;

	unsigned long long totalTime;

	unsigned long long maxTime;
	unsigned  long long minTime;
	unsigned long long callsCount;

	std::vector<unsigned long long> samples;
};
