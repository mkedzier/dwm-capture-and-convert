// Some stupid preprocessing shader; just a placeholder
// for something usable.
Texture2D<float4> BufferIn : register(t0);

RWTexture2D<float> Y: register(u0);
RWTexture2D<float2> UV : register(u1);
RWTexture2D<float> Y1: register(u2);
RWTexture2D<float2> UV1 : register(u3);


[numthreads(32, 32, 1)]
void CSMain(uint3 dispatchThreadID : SV_DispatchThreadID)
{
	uint2 storePos = dispatchThreadID.xy;
	uint2 pos;
	uint2 ypos;
	float4 vr;
	float4 vl;
	float4 vld;
	float4 vrd;
	float2 sumUV;
	float4 yvec;
	float y0, u0, v0, y1, u1, v1, y2, u2, v2, y3, u3, v3;

	if ((storePos.x % 2) == 0 && (storePos.y % 2) == 0)
	{
		pos = storePos;
		ypos = storePos;
		vl = BufferIn[storePos.xy];
		storePos.x += 1;
		vr = BufferIn[storePos.xy];
		storePos.y += 1;
		vrd = BufferIn[storePos.xy];
		storePos.x -= 1;
		vld = BufferIn[storePos.xy];

		vl.x = vl.x*256.0f;
		vl.y = vl.y*256.0f;
		vl.z = vl.z*256.0f;

		vr.x = vr.x*256.0f;
		vr.y = vr.y*256.0f;
		vr.z = vr.z*256.0f;

		vld.x = vld.x*256.0f;
		vld.y = vld.y*256.0f;
		vld.z = vld.z*256.0f;

		vrd.x = vrd.x*256.0;
		vrd.y = vrd.y*256.0;
		vrd.z = vrd.z*256.0;

		y0 = ((66 * vl.x + 129 * vl.y + 25 * vl.z + 128) / 256.0) + 16;
		u0 = ((-38 * vl.x - 74 * vl.y + 112 * vl.z + 128) / 256.0) + 128;
		v0 = ((112 * vl.x - 94 * vl.y - 18 * vl.z + 128) / 256.0) + 128;;

		y1 = ((66 * vr.x + 129 * vr.y + 25 * vr.z + 128) / 256.0) + 16;
		u1 = ((-38 * vr.x - 74 * vr.y + 112 * vr.z + 128) / 256.0) + 128;
		v1 = ((112 * vr.x - 94 * vr.y - 18 * vr.z + 128) / 256.0) + 128;

		y2 = ((66 * vrd.x + 129 * vrd.y + 25 * vrd.z + 128) / 256.0) + 16;
		u2 = ((-38 * vrd.x - 74 * vrd.y + 112 * vrd.z + 128) / 256.0) + 128;
		v2 = ((112 * vrd.x - 94 * vrd.y - 18 * vrd.z + 128) / 256.0) + 128;

		y3 = ((66 * vld.x + 129 * vld.y + 25 * vld.z + 128) / 256.0) + 16;
		u3 = ((-38 * vld.x - 74 * vld.y + 112 * vld.z + 128) / 256.0) + 128;
		v3 = ((112 * vld.x - 94 * vld.y - 18 * vld.z + 128) / 256.0) + 128;

		sumUV.x = (u0 + u1 + u2 + u3) / 4;
		sumUV.y = (v0 + v1 + v2 + v3) / 4;

		sumUV.x = sumUV.x / 256.0;
		sumUV.y = sumUV.y / 256.0;

		pos.x = pos.x / 2;
		pos.y = pos.y / 2;
		UV[pos.xy] = sumUV.xy;
		UV1[pos.xy] = sumUV.xy;

		yvec.x = y0 / 256.0;
		Y[ypos.xy] = yvec.x;
		Y1[pos.xy] = yvec.x;
		ypos.x = ypos.x + 1;
		yvec.x = y1 / 256.0;
		Y[ypos.xy] = yvec.x;
		Y1[ypos.xy] = yvec.x;

		ypos.y = ypos.y + 1;
		yvec.x = y2 / 256.0;
		Y[ypos.xy] = yvec.x;
		Y1[ypos.xy] = yvec.x;

		ypos.x = ypos.x - 1;
		yvec.x = y3 / 256.0;
		Y[ypos.xy] = yvec.x;
		Y1[ypos.xy] = yvec.x;

	}
}
