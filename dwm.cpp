#include "pch.h"
#include <iostream>
#include <iomanip>
#include <cstdio>

#include <windows.h>
#include <d3d11.h>
#include <dxgi1_2.h>
#include <d3dcompiler.h>

#include <chrono> 
#include <iostream>
#include <vector>

#pragma comment (lib, "d3d11.lib")
#pragma comment (lib, "D3DCompiler.lib")

#include <assert.h>

#include "cpu_profiler.h"
#include "gpu_profiler.h"

ID3D11Device *Device;
ID3D11DeviceContext *Context;
ID3D11Texture2D *TemporaryDesktopTexture;
ID3D11Texture2D *RGBATextureCpuAccess;
ID3D11Texture2D *NV12Texture;
#ifdef POSTPROCESSBUFFER
ID3D11Texture2D *NV12TexturePostProcessed;
ID3D11UnorderedAccessView *NV12LumaUAV1;
ID3D11UnorderedAccessView *NV12ChromaUAV1;
#endif
ID3D11Texture2D *NV12TextureCpuAccess;
ID3D11ShaderResourceView *srcGPUView;

IDXGIOutputDuplication *DeskDupl;
ID3D11Texture2D *AcquiredDesktopImage;

ID3D11UnorderedAccessView *NV12LumaUAV;
ID3D11UnorderedAccessView *NV12ChromaUAV;

ID3D11Query *Query;

bool DumpNV12 = false;
bool DumpBMP = false;
bool MeasureCaptureOnly = false;
bool SeparateBitmapDumps = false;

bool LoadComputeShader(LPCWSTR filename, ID3D11ComputeShader** computeShader)
{
    DWORD flags = D3DCOMPILE_ENABLE_STRICTNESS;
    ID3DBlob* blobError = nullptr;
    ID3DBlob* blob = nullptr;

#if defined( _DEBUG )
    flags |= D3DCOMPILE_DEBUG;
#endif

    HRESULT hr = D3DCompileFromFile(filename, NULL, NULL, "CSMain", "cs_5_0", flags, NULL, &blob, &blobError);
    if (FAILED(hr))
    {
        if (blobError)
            printf("%s \n",(char*)blobError->GetBufferPointer());
        if (blobError)
            blobError->Release();
        if (blobError)
            blobError->Release();
        return false;
    }
    else
    {
        hr = Device->CreateComputeShader(blob->GetBufferPointer(), blob->GetBufferSize(), NULL, computeShader);
        if (blobError)
            blobError->Release();
        if (blobError)
            blobError->Release();

        return hr == S_OK;
    }
}

// Dump output of target texture to file(RGB bmp);
void DumpRGBAtoRGBbmp(const char *FileName, PBYTE ImageBuffer, unsigned int Stride, int Width, int Height)
{
    BITMAPFILEHEADER header;
    BITMAPINFO info;
    DWORD imageSize = Width * Height * 3;

    memset(&header, 0, sizeof(header));
    memset(&info, 0, sizeof(info));

    header.bfType = (('M' << 8) | ('B'));
    header.bfSize = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFO) + imageSize;
    header.bfOffBits = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFO);
 
    info.bmiHeader.biSize = sizeof(info.bmiHeader);
    info.bmiHeader.biWidth = Width;
    info.bmiHeader.biHeight = (Height < 0 ? Height : -Height);
    info.bmiHeader.biPlanes = 1;
    info.bmiHeader.biBitCount = 24;
    info.bmiHeader.biCompression = BI_RGB;
    info.bmiHeader.biSizeImage = imageSize;
   
    FILE *file;
    errno_t result = fopen_s(&file, FileName, "wb");
    if (result == 0)
    {
        fwrite((const char*)&header, 1, sizeof(BITMAPFILEHEADER), file);
        fwrite((const char*)&info, 1, sizeof(BITMAPINFO), file);

        for (int i = 0; i < Height; i++)
        {
            for (int k = 0; k < Width; k++)
            {
				UINT pixelOffset = i * Stride + k * 4;

                fwrite(&ImageBuffer[pixelOffset + 2], 1, 1, file);
                fwrite(&ImageBuffer[pixelOffset + 1], 1, 1, file);
                fwrite(&ImageBuffer[pixelOffset], 1, 1, file);
            }
        }
        fclose(file);
    }
}



bool CreateNV12TexturesAndViews(UINT Width, UINT Height)
{
    D3D11_TEXTURE2D_DESC desc = CD3D11_TEXTURE2D_DESC(
        DXGI_FORMAT_NV12,
        Width,
        Height,
        1,
        1,
        D3D11_BIND_UNORDERED_ACCESS,
        D3D11_USAGE_DEFAULT,
        0
    );

    D3D11_TEXTURE2D_DESC descCpuAccess = CD3D11_TEXTURE2D_DESC(
        DXGI_FORMAT_NV12,
        Width,
        Height,
        1,
        1,
        0,
        D3D11_USAGE_STAGING,
        D3D11_CPU_ACCESS_READ
    );

    CD3D11_UNORDERED_ACCESS_VIEW_DESC yDesc = CD3D11_UNORDERED_ACCESS_VIEW_DESC(
        NV12Texture,
        D3D11_UAV_DIMENSION_TEXTURE2D,
        DXGI_FORMAT_R8_UNORM
    );

    CD3D11_UNORDERED_ACCESS_VIEW_DESC uvDesc = CD3D11_UNORDERED_ACCESS_VIEW_DESC(
        NV12Texture,
        D3D11_UAV_DIMENSION_TEXTURE2D,
        DXGI_FORMAT_R8G8_UNORM
    );

	HRESULT hr = S_OK;
#ifdef POSTPROCESSBUFFER

	hr = Device->CreateTexture2D(
		&desc,
		nullptr,
		&NV12TexturePostProcessed);

	if (FAILED(hr))
	{
		return false;
	}

	hr = Device->CreateUnorderedAccessView(
		NV12TexturePostProcessed,
		&yDesc,
		&NV12LumaUAV1
	);

	if (FAILED(hr))
	{		
		return false;
	}

	hr = Device->CreateUnorderedAccessView(
		NV12TexturePostProcessed,
		&uvDesc,
		&NV12ChromaUAV1
	);

	if (FAILED(hr))
	{
		return false;
	}

#endif

    hr = Device->CreateTexture2D(
        &desc,
        nullptr,
        &NV12Texture);

    if (FAILED(hr))
    {
        return false;
    }

    hr = Device->CreateTexture2D(
        &descCpuAccess,
        nullptr,
        &NV12TextureCpuAccess);

    if (FAILED(hr))
    {
        NV12Texture->Release();
        return false;
    }

    hr = Device->CreateUnorderedAccessView(
        NV12Texture,
        &yDesc,
        &NV12LumaUAV
    );

    if (FAILED(hr))
    {
        NV12Texture->Release();
        NV12TextureCpuAccess->Release();
        return false;
    }

    hr = Device->CreateUnorderedAccessView(
        NV12Texture,
        &uvDesc,
        &NV12ChromaUAV
    );
    
    if (FAILED(hr))
    {
        NV12Texture->Release();
        NV12TextureCpuAccess->Release();
        NV12LumaUAV->Release();
        return false;
    }

    return true;
}

void SetUpDWMApiAndMeasure(UINT NumberOfFrames)
{
    HRESULT hr = S_OK;
    UINT flags = 0;

    D3D_DRIVER_TYPE DriverTypes[] =
    {
        D3D_DRIVER_TYPE_HARDWARE,
        D3D_DRIVER_TYPE_WARP,
        D3D_DRIVER_TYPE_REFERENCE,
    };
    UINT NumDriverTypes = ARRAYSIZE(DriverTypes);

    // Feature levels supported
    D3D_FEATURE_LEVEL FeatureLevels[] =
    {
        D3D_FEATURE_LEVEL_11_0,
        D3D_FEATURE_LEVEL_10_1,
        D3D_FEATURE_LEVEL_10_0,
        D3D_FEATURE_LEVEL_9_1
    };
    UINT NumFeatureLevels = ARRAYSIZE(FeatureLevels);

    D3D_FEATURE_LEVEL FeatureLevel;

#ifdef _DEBUG
    flags = D3D11_CREATE_DEVICE_DEBUG;
#endif

    for (UINT DriverTypeIndex = 0; DriverTypeIndex < NumDriverTypes; DriverTypeIndex++)
    {
        hr = D3D11CreateDevice(nullptr, DriverTypes[DriverTypeIndex], nullptr, flags, FeatureLevels, NumFeatureLevels,
            D3D11_SDK_VERSION, &Device, &FeatureLevel, &Context);
        if (SUCCEEDED(hr))
        {            
            break;
        }
    }

    D3D11_QUERY_DESC queryDesc;
    queryDesc.Query = D3D11_QUERY_TIMESTAMP;
    queryDesc.MiscFlags = 0;

    hr = Device->CreateQuery(&queryDesc, &Query);
    if (FAILED(hr))
    {
        printf("DxgiOutput->QueryInterface failed [0x%x] \n", hr);
        return;
    }

    IDXGIDevice* DxgiDevice = nullptr;
    hr = Device->QueryInterface(__uuidof(IDXGIDevice), reinterpret_cast<void**>(&DxgiDevice));
    if (FAILED(hr))
    {
        return;
    }

    IDXGIAdapter* DxgiAdapter = nullptr;
    hr = DxgiDevice->GetParent(__uuidof(IDXGIAdapter), reinterpret_cast<void**>(&DxgiAdapter));
    DxgiDevice->Release();
    DxgiDevice = nullptr;
    if (FAILED(hr))
    {
        return;
    }

    RECT DesktopBounds;

    DesktopBounds.left = INT_MAX;
    DesktopBounds.right = INT_MIN;
    DesktopBounds.top = INT_MAX;
    DesktopBounds.bottom = INT_MIN;

    IDXGIOutput* DxgiOutput = nullptr;
   
    UINT currentOutput = 0;
    INT SingleOutput = -1;
    DXGI_OUTPUT_DESC DesktopDesc;
    hr = S_OK;

    // Create desktop duplication for only one (first) output (aka monitor)
    for(int i = 0; i<1; i++)
    {
        hr = DxgiAdapter->EnumOutputs(i, &DxgiOutput);
        if (hr != DXGI_ERROR_NOT_FOUND)
        {
            DxgiOutput->GetDesc(&DesktopDesc);

            DesktopBounds.left = min(DesktopDesc.DesktopCoordinates.left, DesktopBounds.left);
            DesktopBounds.top = min(DesktopDesc.DesktopCoordinates.top, DesktopBounds.top);
            DesktopBounds.right = max(DesktopDesc.DesktopCoordinates.right, DesktopBounds.right);
            DesktopBounds.bottom = max(DesktopDesc.DesktopCoordinates.bottom, DesktopBounds.bottom);

            IDXGIOutput1* DxgiOutput1 = nullptr;
            hr = DxgiOutput->QueryInterface(__uuidof(DxgiOutput1), reinterpret_cast<void**>(&DxgiOutput1));
            DxgiOutput->Release();
            DxgiOutput = nullptr;
            if (FAILED(hr))
            {
                printf("DxgiOutput->QueryInterface failed [0x%x] \n", hr);
                return;
            }

            hr = DxgiOutput1->DuplicateOutput(Device, &DeskDupl);
            if (FAILED(hr))
            {
                printf("DxgiOutput->QueryInterface for __uuidof(DxgiOutput1) failed with [0x%x] \n", hr);
                DxgiOutput1->Release();
                return;
            }

            DxgiOutput1->Release();
            DxgiOutput1 = nullptr;
        }
    }
    
    DxgiAdapter->Release();
    DxgiAdapter = nullptr;

    UINT width = DesktopBounds.right - DesktopBounds.left;
    UINT height = DesktopBounds.bottom - DesktopBounds.top;
    UINT temp = 0;

    // Take rotation into account
    switch (DesktopDesc.Rotation)
    {
        assert(DesktopDesc.Rotation == DXGI_MODE_ROTATION_UNSPECIFIED ||
            DesktopDesc.Rotation == DXGI_MODE_ROTATION_ROTATE90);
        case DXGI_MODE_ROTATION_UNSPECIFIED:
            break;
        case DXGI_MODE_ROTATION_IDENTITY:
            break;
        case DXGI_MODE_ROTATION_ROTATE90:
            temp = width;
            width = height;
            height = temp;
            break;
        case DXGI_MODE_ROTATION_ROTATE180:          
            break;
        case DXGI_MODE_ROTATION_ROTATE270:
            break; 
    }
    
    D3D11_TEXTURE2D_DESC desktopTextureDescription;
    RtlZeroMemory(&desktopTextureDescription, sizeof(D3D11_TEXTURE2D_DESC));
    desktopTextureDescription.Width = width;
    desktopTextureDescription.Height = height;
    desktopTextureDescription.MipLevels = 1;
    desktopTextureDescription.ArraySize = 1;
    desktopTextureDescription.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
    desktopTextureDescription.SampleDesc.Count = 1;
    desktopTextureDescription.Usage = D3D11_USAGE_DEFAULT;
    desktopTextureDescription.BindFlags =  D3D11_BIND_SHADER_RESOURCE;
    desktopTextureDescription.CPUAccessFlags = 0;
    desktopTextureDescription.MiscFlags = 0;

    hr = Device->CreateTexture2D(&desktopTextureDescription, nullptr, &TemporaryDesktopTexture);
    if (FAILED(hr))
    {
        printf("CreateTexture2D failed!\n");
        return;
    }
    
    desktopTextureDescription.BindFlags = 0;
    desktopTextureDescription.Usage = D3D11_USAGE_STAGING;
    desktopTextureDescription.CPUAccessFlags = D3D11_CPU_ACCESS_READ;

    // Create NV12 texture that can be dumped to file
    hr = Device->CreateTexture2D(&desktopTextureDescription, nullptr, &RGBATextureCpuAccess);
    if (FAILED(hr))
    {
        printf("CreateTexture2D failed!\n");
        return;
    }

    D3D11_SHADER_RESOURCE_VIEW_DESC descView;
    ZeroMemory(&descView, sizeof(descView));
    descView.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
    descView.Texture2D.MipLevels = 1;
    descView.Texture2D.MostDetailedMip = 0;
    descView.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
    
    hr = Device->CreateShaderResourceView(TemporaryDesktopTexture, &descView, &srcGPUView);
    if (FAILED(hr))
    {
        printf("CreateShaderResourceView failed!\n");
        return;
    }

    ID3D11ComputeShader *computeShader;

#ifdef POSTPROCESSBUFFER
	// Postprocessing can be used to do some operation on captured 
	// image before moving it color conversion shader
	if (!LoadComputeShader(L"cs_preproc.hlsl", &computeShader))
	{
		printf("Can't load shader!\n");
		return;
	}
#else
    if (!LoadComputeShader(L"cs.hlsl", &computeShader))
    {
        printf("Can't load shader!\n");
        return;
    }
#endif

    if (!CreateNV12TexturesAndViews(width, height))
    {
        printf("Can't create NV12 texture and views\n");
        return;
    }

    IDXGIResource* DesktopResource = nullptr;
    DXGI_OUTDUPL_FRAME_INFO FrameInfo;

    cpu_profiler cpu_prof("cpu         ");
    gpu_profiler gpu_prof("gpu         ", Device, Context);
    gpu_profiler acquire_prof("dsk acquire ", Device, Context);
    gpu_profiler copy_prof("dsk copy    ", Device, Context);
    gpu_profiler conversion_prof("rgb->nv12   ", Device, Context);

    DXGI_OUTDUPL_DESC outputDuplDesc;
    DeskDupl->GetDesc(&outputDuplDesc);

    printf("Desktop in GPU memory: %s \n", 
        outputDuplDesc.DesktopImageInSystemMemory ? "no" : "yes");
    
    char fileName[255];
    static FILE *f = nullptr;
#ifdef POSTPROCESSBUFFER
	static FILE *f0 = nullptr;
#endif
    // Process particular frames
    for (UINT i = 0; i < NumberOfFrames; i++)
    {
        // Note: before running the code make sure that 
        // there are updates on the screen (just start playing movie etc.); 
        // otherwise measurements are not going to be realistic. For example 
        // when user moves mouse pointer AcquireNextFrame returns 
        // AccumulatedFrames == 0,  TotalMetadataBufferSize == 0, and LastPresentTime == 0

        gpu_prof.start();
        cpu_prof.start();

        acquire_prof.start();
       
		// Grab next frame; timeout is set to 0.5 second
        HRESULT hr = DeskDupl->AcquireNextFrame(500, &FrameInfo, &DesktopResource);

        acquire_prof.end();

        if (FAILED(hr))
        {
            if (hr == DXGI_ERROR_WAIT_TIMEOUT)
            {
                printf("AcquireNextFrame failed = timout!\n");
                gpu_prof.end();
                cpu_prof.end();
                continue;
            }
            else
            {
                printf("AcquireNextFrame failed = [0x%x] \n", hr);
                return;
            }
        }
        
        if (FrameInfo.LastPresentTime.QuadPart == 0)
        {
            printf("No update!\n");
        }

        if (FrameInfo.LastPresentTime.QuadPart == 0 && 
            FrameInfo.AccumulatedFrames == 0 &&
            FrameInfo.TotalMetadataBufferSize == 0)
        {
            printf("Only mouse info updated! \n");
            hr = DeskDupl->ReleaseFrame();
            gpu_prof.end();
            cpu_prof.end();
            continue;
        }

        if (MeasureCaptureOnly)
        {
            hr = DeskDupl->ReleaseFrame();
            gpu_prof.end();
            cpu_prof.end();
            continue;
        }

        hr = DesktopResource->QueryInterface(__uuidof(ID3D11Texture2D), reinterpret_cast<void **>(&AcquiredDesktopImage));
        if (FAILED(hr))
        {
            printf("DesktopResource->QueryInterface for __uuidof(ID3D11Texture2D) failed!\n");
            return;
        }
       
        copy_prof.start();

        // Do a copy of the acquired image for further processing by the shader.
		// Note: this step can be avoided.
        Context->CopyResource(TemporaryDesktopTexture, AcquiredDesktopImage);
        
#if 0
        D3D11_BOX sourceRegion;
        sourceRegion.left = 0;
        sourceRegion.right = width;
        sourceRegion.top = 0;
        sourceRegion.bottom = height;
        sourceRegion.front = 0;
        sourceRegion.back = 1;

        Context->CopySubresourceRegion(TemporaryDesktopTexture,
            0,
            0,
            0,
            0,
            AcquiredDesktopImage,
            0,
            &sourceRegion);
#endif

        copy_prof.end();

        AcquiredDesktopImage->Release();
        DesktopResource->Release();
        DesktopResource = nullptr;
        hr = DeskDupl->ReleaseFrame();
        if (FAILED(hr))
        {
            printf("DeskDupl->ReleaseFrame() failed!\n");
            return;
        }
       
#ifdef POSTPROCESSBUFFER

		ID3D11UnorderedAccessView* uavempty[4] = { NULL, NULL, NULL, NULL };
		ID3D11ShaderResourceView*  srvempty[1] = { NULL };
		ID3D11UnorderedAccessView* uav[4] = { NV12LumaUAV, NV12ChromaUAV, NV12LumaUAV1, NV12ChromaUAV1};

		Context->CSSetShader(computeShader, NULL, 0);

		// Set resource and views
		Context->CSSetShaderResources(0, 1, &srcGPUView);
		Context->CSSetUnorderedAccessViews(0, 4, uav, NULL);

		p2.start();
		// Execute shader
		Context->Dispatch(width / 32, height / 32, 1);
		p2.end();

		// Unset shader resource and views
		Context->CSSetShader(NULL, NULL, 0);
		Context->CSSetShaderResources(0, 1, srvempty);
		Context->CSSetUnorderedAccessViews(0, 4, uavempty, NULL);

#else

		ID3D11UnorderedAccessView* uavempty[2] = { NULL, NULL };
		ID3D11ShaderResourceView*  srvempty[1] = { NULL };
		ID3D11UnorderedAccessView* uav[2] = { NV12LumaUAV, NV12ChromaUAV };

        Context->CSSetShader(computeShader, NULL, 0);
        
        // Set resource and views
        Context->CSSetShaderResources(0, 1, &srcGPUView);
        Context->CSSetUnorderedAccessViews(0, 2, uav, NULL);
        
        conversion_prof.start();
        // Execute shader
        Context->Dispatch(width / 32, height / 32, 1);
        conversion_prof.end();

        // Unset shader resource and views
        Context->CSSetShader(NULL, NULL, 0);
        Context->CSSetShaderResources(0, 1, srvempty);
        Context->CSSetUnorderedAccessViews(0, 2, uavempty, NULL);
#endif

        gpu_prof.end();
        cpu_prof.end();

        // Dump NV12 or BMP files on disk
        if (DumpNV12)
        {
            // Copy NV12 surface to CPU accessible surface
            Context->CopyResource(NV12TextureCpuAccess, NV12Texture);
			
            D3D11_MAPPED_SUBRESOURCE mappedSubresource;
            hr = Context->Map(NV12TextureCpuAccess, 0, D3D11_MAP_READ, 0, &mappedSubresource);
            if (FAILED(hr))
            {
                printf("Failed mapping image!\n");
                return;
            }
            else
            {

                if (SeparateBitmapDumps)
                {
                    sprintf(fileName, "desktop_%d_%d_%d.nv12", i, width, height);

                    f = fopen(fileName, "wb");
                    if (f)
                    {
                        fwrite(mappedSubresource.pData, 1, mappedSubresource.DepthPitch, f);
                        fclose(f);
                        f = nullptr;
                    }
                }
                else
                {
                    if (!f)
                    {
                        sprintf(fileName, "dump_%d_%d.nv12", width, height);
                        f = fopen(fileName, "wb");
                    }
                    
					fwrite(mappedSubresource.pData, 1, mappedSubresource.DepthPitch, f);
                }
                Context->Unmap(NV12TextureCpuAccess, 0);
            }

#ifdef POSTPROCESSBUFFER

			// Copy NV12 surface to CPU accessible surface
			Context->CopyResource(NV12TextureCpuAccess, NV12TexturePostProcessed);

			hr = Context->Map(NV12TextureCpuAccess, 0, D3D11_MAP_READ, 0, &mappedSubresource);
			if (FAILED(hr))
			{
				printf("Failed mapping image!\n");
				return;
			}
			else
			{
				if (SeparateBitmapDumps)
				{
					sprintf(fileName, "desktop_%d_%d_%d_preproc.nv12", i, width, height);

					f = fopen(fileName, "wb");
					if (f)
					{
						fwrite(mappedSubresource.pData, 1, mappedSubresource.DepthPitch, f);
						fclose(f);
						f = nullptr;
					}
				}
				else
				{
					if (!f0)
					{
						sprintf(fileName, "dump_%d_%d_preproc.nv12", width, height);
						f0 = fopen(fileName, "wb");
					}

					fwrite(mappedSubresource.pData, 1, mappedSubresource.DepthPitch, f0);
				}
				Context->Unmap(NV12TextureCpuAccess, 0);
			}

#endif
        }

		if (DumpBMP)
		{
			Context->CopyResource(RGBATextureCpuAccess, TemporaryDesktopTexture);
			
			D3D11_MAPPED_SUBRESOURCE mappedSubresource;
			hr = Context->Map(RGBATextureCpuAccess, 0, D3D11_MAP_READ, 0, &mappedSubresource);
			if (FAILED(hr))
			{
				printf("Failed mapping image!\n");
				return;
			}
			else
			{
				sprintf(fileName, "desktop_%d_%d_%d.bmp", i, width, height);

				DumpRGBAtoRGBbmp(fileName, (PBYTE)mappedSubresource.pData, mappedSubresource.RowPitch, width, height);
				
				Context->Unmap(RGBATextureCpuAccess, 0);
			}
		}

    }
    if (f)
        fclose(f);

#ifdef POSTPROCESSBUFFER
	if (f0)
		fclose(f0);
#endif

    gpu_prof.dump_ms();
    gpu_prof.dump_results_to_file();

    cpu_prof.dump();
    cpu_prof.dump_results_to_file();

    acquire_prof.dump_ms();
    acquire_prof.dump_results_to_file();

    copy_prof.dump_ms();
    copy_prof.dump_results_to_file();

    conversion_prof.dump_ms();
    conversion_prof.dump_results_to_file();

    return;
}

void ShowBanner(char *ProgramName)
{
	printf("options: %s number_of_frames -dumpnv12 {combined | separate} | -dumpbmp | -measure_capture_only\n", ProgramName);
}

int main(int argc, char *argv[])
{   
	UINT numberOfFrames = 1;
	if (argc > 1)
	{
		numberOfFrames = atoi(argv[1]);
		if (numberOfFrames == 0)
		{
			ShowBanner(argv[0]);
			return 1;
		}

		if (strcmp(argv[2], "-dumpnv12") == 0)
		{
			DumpNV12 = true;
			if (argc >= 4)
			{
				if (strcmp(argv[3], "separate") == 0)
				{
					SeparateBitmapDumps = true;
				}
				else
					if (strcmp(argv[3], "combined") == 0)
					{
						SeparateBitmapDumps = false;
					}
			}
		}
		else
		if (strcmp(argv[2], "-measure_capture_only") == 0)
		{
			printf("Measuring only desktop capture!\n");
			MeasureCaptureOnly = true;
		}
		else
		if (strcmp(argv[2], "-dumpbmp") == 0)
		{
			printf("Dumping frames to RGB file!\n");
			MeasureCaptureOnly = true;
		}
		else		
		{
			ShowBanner(argv[0]);
			return 1;
		}
	}
	else
	{
		ShowBanner(argv[0]);
		return 1;
	}
	printf("press enter to start...\n");
	getchar();
    SetUpDWMApiAndMeasure(numberOfFrames);
	printf("exiting...\n");
	getchar();
	printf("exited...\n");
}