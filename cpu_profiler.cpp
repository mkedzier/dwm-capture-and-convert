#include "pch.h"
#include "cpu_profiler.h"

cpu_profiler::cpu_profiler(std::string Name)
{
	name = Name;
	callsCount = 0;
	totalTime = 0;
	maxTime = 0;

	minTime = ~0;
	maxTime = 0;
}

void cpu_profiler::start()
{
	begin = std::chrono::steady_clock::now();
}

void cpu_profiler::end()
{
	endTime = std::chrono::steady_clock::now();
	unsigned long long  diff = std::chrono::duration_cast<std::chrono::nanoseconds>(endTime - begin).count();
	if (diff > maxTime)
	{
		maxTime = diff;
	}

	if (diff < minTime)
	{
		minTime = diff;
	}

	totalTime = totalTime + diff;
	samples.push_back(diff);
	callsCount++;
}

long long cpu_profiler::get_total_time() const
{
	return totalTime;
}

void cpu_profiler::reset()
{
	callsCount = 0;
	totalTime = 0;
	maxTime = 0;

	minTime = ~0;
	maxTime = 0;
}

void cpu_profiler::dump()
{
	unsigned long long totalTimeInMs = totalTime / 1000000;

	unsigned long long avg;
	if (callsCount == 0)
	{
		avg = totalTimeInMs;
	}
	else
	{
		avg = totalTimeInMs / callsCount;
	}
	unsigned long long minTimeInMs = minTime / 1000000;
	unsigned long long maxTimeInMs = maxTime / 1000000;

	std::cout << name.c_str() << ":time ms[" << totalTimeInMs << "]" << " avg [" << avg << "]" << " min [" << minTimeInMs << "]" << " max[" << maxTimeInMs << "]" << " called[" << callsCount << "]" << std::endl;
}

void cpu_profiler::dump_results_to_file()
{
	std::string fileName = name + ".txt";

	FILE *f = fopen(fileName.c_str(), "w");
	if (f)
	{
		for (size_t i = 0; i < samples.size(); i++)
		{
			fprintf(f, "%.2f \n", ((double)samples[i]) / 1000000.0);
		}
		fclose(f);
	}
}