#include "pch.h"
#include "gpu_profiler.h"

gpu_profiler::gpu_profiler(std::string ProfilerName, ID3D11Device *Device, ID3D11DeviceContext *Context)
{
	freqQuery = nullptr;
	startQuery = nullptr;
	endQuery = nullptr;
	device = Device;

	device->AddRef();
	context = Context;
	context->AddRef();

	minTime = LLONG_MAX;
	maxTime = 0;

	name = ProfilerName;

	startTime = 0;
	endTime = 0;

	totalTime = 0;
	callsCount = 0;

	D3D11_QUERY_DESC desc;
	desc.Query = D3D11_QUERY_TIMESTAMP_DISJOINT;
	desc.MiscFlags = 0;

	// Create query
	HRESULT hr = Device->CreateQuery(&desc, &freqQuery);

	// Create begin and end timers
	desc.Query = D3D11_QUERY_TIMESTAMP;
	hr = Device->CreateQuery(&desc, &startQuery);
	hr = Device->CreateQuery(&desc, &endQuery);
}

// Start of profiling GPU code
void gpu_profiler::start()
{
	context->Begin(freqQuery);
	context->End(startQuery);

	while (S_OK != context->GetData(startQuery, &startTime, sizeof(startTime), 0))
	{

	}
}

// End of profiling GPU code
void gpu_profiler::end()
{
	context->End(endQuery);
	while (S_OK != context->GetData(endQuery, &endTime, sizeof(endTime), 0))
	{

	}

	context->End(freqQuery);
	while (S_OK != context->GetData(freqQuery, &timeStamp, sizeof(timeStamp), 0))
	{

	}

	INT64 diff = endTime - startTime;
	if (diff > maxTime)
	{
		maxTime = diff;
	}

	if (diff < minTime)
	{
		minTime = diff;
	}

	totalTime = totalTime + diff;

	samples.push_back(diff);

	callsCount++;

}

// Dump measurements in milliseconds (floating point numbers)
void gpu_profiler::dump_ms()
{
	if (callsCount == 0)
	{
		std::cout << name.c_str() << ":not used!" << std::endl;
		return;
	}
	double t = ((double)totalTime) / ((double)timeStamp.Frequency);

	double totalTimeInMs = (t * 1000.0);
	double avgTimeInMs = ((t / callsCount) * 1000.0);

	double minTimeInMs = (((double)minTime) / ((double)timeStamp.Frequency)) * 1000;
	double maxTimeInMs = (((double)maxTime) / ((double)timeStamp.Frequency)) * 1000;

	std::cout << std::fixed << std::setprecision(2);
	std::cout << name.c_str() << ":time ms[" << totalTimeInMs << "]" << " avg [" << avgTimeInMs << "]" << " min [" << minTimeInMs << "]" << " max[" << maxTimeInMs << "]" << " called[" << callsCount << "]" << std::endl;
}

//
void gpu_profiler::dump()
{
	if (callsCount == 0)
	{
		std::cout << name.c_str() << ":not used!" << std::endl;
		return;
	}

	double totalTimeInSeconds = (((double)totalTime) / ((double)timeStamp.Frequency));
	double minTimeInSeconds = (((double)minTime) / ((double)timeStamp.Frequency));
	double maxTimeInSeconds = (((double)maxTime) / ((double)timeStamp.Frequency));

	std::cout << name.c_str() << ":time ms[" << totalTimeInSeconds << "]" << " avg [" << 0 << "]" << " min [" << minTimeInSeconds << "]" << " max[" << maxTimeInSeconds << "]" << " called[" << callsCount << "]" << std::endl;
}

// Dump samples to file as floating point numbers
void gpu_profiler::dump_results_to_file()
{
	if (callsCount == 0)
	{
		return;
	}

	std::string fileName = name + ".txt";

	FILE *f = fopen(fileName.c_str(), "w");
	if (f)
	{
		for (size_t i = 0; i < samples.size(); i++)
		{
			INT64 s = samples[i];

			double t = ((double)s) / ((double)timeStamp.Frequency);
			double timeInMs = (t * 1000.0);

			fprintf(f, "%.2f\n", timeInMs);
		}
		fclose(f);
	}
}

gpu_profiler::~gpu_profiler()
{
	if (endQuery)
	{
		endQuery->Release();
	}
	if (startQuery)
	{
		startQuery->Release();
	}
	if (freqQuery)
	{
		freqQuery->Release();
	}

	if (context)
	{
		context->Release();
	}

	if (device)
	{
		device->Release();
	}
}
