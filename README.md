dmw_test is a simple tool for grabbing desktop using DWM API (Windows) and then converting 
desktop image (RGBA) into NV12 format using DX11 compute shader.

The tool measures CPU and GPU time during capturing and converting the desktop image.
It measures every point of the pipeline and displays stats in milliseconds.
It can also dump the results of conversion in NV12 format.

The whole project can be used in any kind of desktop streaming solution. The screen 
that is captured can be passed to any separate encoding API that can consume 
DirectX11 texture directly from GPU memory (or through mapping) and generate an 
encoded stream (h264, vp8, vp9, etc.).

The tool ignores any additional information (like dirty rectangles/mouse movements) 
provided by the API and just grabs the whole desktop screen.

In order to make measurements realistic, please run any application that is generating 
screen updates (like playing a clip in the youtube) and make this application visible!
The idea is to ignore screen capturing timeouts.

For additional information consult DWM API in MSDN or provided source code.

Tool syntax:

dmw_test.exe number_of_frames -dumpnv12 { combined | separate } | -dumpbmp | -measure_capture_only

Sample syntax:

dwm_test.exe 60 -measure_capture_only 

will grab 60 frames and will measure capturing time only.

dmw_test.exe 60 -dumpnv12 combined

will grab 60 frames and will convert them to NV12 and dump them to one, combined file.

dmw_test.exe 60 -dumpnv12 separate

will grab 60 frames and will convert them to separate files. 


 